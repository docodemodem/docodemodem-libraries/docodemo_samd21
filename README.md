# docodemo_samd21
サーキットデザイン製プログラマブル無線ユニット「どこでもでむmini」用のライブラリです。

FreeRTOSはこちらを利用しています。
https://github.com/BriscoeTech/Arduino-FreeRTOS-SAMD21

platformioで新プロジェクトを作るときは" Arduino M0"を指定してください。

## 注意事項

Arduinoとのハードウェアの違いに対応するため、下記フォルダにあるvariant.hとvariant.cppとUart.cppを変更してください。

> C:\Users\\********\\.platformio\packages\framework-arduino-samd\variants\arduino_mzero\variant.h

> C:\Users\\********\\.platformio\packages\framework-arduino-samd\variants\arduino_mzero\variant.cpp

> C:\Users\\********\\.platformio\packages\framework-arduino-samd\cores\arduino\Uart.cpp


1.variant.hの変更部分

LED宣言関連をコメントアウトする。
```
// LEDs
#if 0
#define PIN_LED_13           (13u)
#define PIN_LED_RXL          (30u)
#define PIN_LED_TXL          (31u)
#define PIN_LED              PIN_LED_13
#define PIN_LED2             PIN_LED_RXL
#define PIN_LED3             PIN_LED_TXL
#define LED_BUILTIN          PIN_LED_13
#endif
```


2.variant.cppの変更部分

*外部I2Cを使用するために下記部分を、
```
 { PORTA,  8, PIO_TIMER,
 { PORTA,  9, PIO_TIMER,
```
下記に変更
```
 { PORTA,  8, PIO_SERCOM_ALT,
 { PORTA,  9, PIO_SERCOM_ALT, 
```


3.Uart.cppの変更部分

*LTE-MモジュールでCTSを使うために下記２か所を変更する。

void Uart::IrqHandler()関数内の下記部分を
```
  if (sercom->isDataRegisterEmptyUART()) {
    if (txBuffer.available()) {
      uint8_t data = txBuffer.read_char();

      sercom->writeDataUART(data);
    } else {
      sercom->disableDataRegisterEmptyInterruptUART();
    }
  }

```

下記に変更
```
  if (sercom->isDataRegisterEmptyUART()) {
    if (txBuffer.available()) {
      uint8_t data = txBuffer.read_char();
#ifdef USE_LTE_M_CTS
    if(uc_pinRX == 0){
        while(digitalRead(8));//RF_CTS_IN
    }
#endif
      sercom->writeDataUART(data);
    } else {
      sercom->disableDataRegisterEmptyInterruptUART();
    }
  }
```

size_t Uart::write(const uint8_t data)関数内の下記部分を
```
  if (sercom->isDataRegisterEmptyUART() && txBuffer.available() == 0) {
    sercom->writeDataUART(data);
  } else {
```

下記に変更
```
  if (sercom->isDataRegisterEmptyUART() && txBuffer.available() == 0) {
#ifdef USE_LTE_M_CTS
    if(uc_pinRX == 0){
        while(digitalRead(8));//RF_CTS_IN
    }
#endif
    sercom->writeDataUART(data);
  } else {
```
whileで止まってしまうので、よいやり方ではありません。ご自身のエラー処理に合わせて変更ください。
<br><br>
LTE-Mを使う場合は、platformio.iniに下記を追記してください。
```
build_flags = -D USE_LTE_M_CTS
```
