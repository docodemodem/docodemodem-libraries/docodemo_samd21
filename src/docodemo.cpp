/*
 * Copyright (c) 2021 Circuit Desgin,Inc
 * Released under the MIT license
 *
 * reference codes
 * https://github.com/adafruit/Adafruit_SleepyDog
 */

#include "docodemo.h"

MCP3221 adcDm = MCP3221();
RX8130 rtcDm = RX8130();
BME280 SensorDm = BME280();

SemaphoreHandle_t xSemaphoreDocodemo = NULL;
const TickType_t xTicksToWait = 500UL;

// USB Uart      : SerialUSB/SerialDebug
// Modem UART    : Serial1/UartModem
// External UART : Serial/UartExternal
// Internal I2C  : Wire/I2C_internal

// External I2C
// variant.cpp and variant.h should be modify
// https://gitlab.com/docodemodem/docodemodem-libraries/docodemo_samd21
TwoWire I2C_external(&sercom2, EXTERNAL_SDA, EXTERNAL_SCL);
void SERCOM2_Handler()
{
    I2C_external.onService();
}

DOCODEMO::DOCODEMO()
{
    _wdt_initialized = false;
    _sleep_initialized = false;
}

bool DOCODEMO::begin()
{
    xSemaphoreDocodemo = xSemaphoreCreateBinary();
    configASSERT(xSemaphoreDocodemo);
    xSemaphoreGive(xSemaphoreDocodemo);

    // Port setting

    // Power
    // digitalWrite(MAIN_PWR_CNT_OUT, LOW);
    pinMode(MAIN_PWR_CNT_OUT, INPUT);

    digitalWrite(EX_POWER_PORT, LOW);
    pinMode(EX_POWER_PORT, OUTPUT);

    digitalWrite(MODEM_POWER_PORT, LOW);
    pinMode(MODEM_POWER_PORT, OUTPUT);

#ifdef MINI_V2
    digitalWrite(EX_UART_POWER_PORT, LOW);
    pinMode(EX_UART_POWER_PORT, OUTPUT);

    digitalWrite(EX_I2C_POWER_PORT, LOW);
    pinMode(EX_I2C_POWER_PORT, OUTPUT);
#else
    digitalWrite(BATMON_ONOFF, LOW);
    pinMode(BATMON_ONOFF, OUTPUT);

    digitalWrite(EXCOM_ONOFF, LOW);
    pinMode(EXCOM_ONOFF, OUTPUT);

    digitalWrite(SD_PW, LOW);
    pinMode(SD_PW, OUTPUT);
#endif
    // GPIO
    digitalWrite(EXGPIO_PORT_OUTPUT_0, LOW);
    pinMode(EXGPIO_PORT_OUTPUT_0, OUTPUT);
    pinMode(EXGPIO_PORT_INPUT_0, INPUT);

    // LED
    digitalWrite(RED_LED, LOW);
    pinMode(RED_LED, OUTPUT);

    digitalWrite(GREEN_LED, LOW);
    pinMode(GREEN_LED, OUTPUT);

    // Volume
#ifdef MINI_V2
    digitalWrite(VOLUME_EN2, LOW);
    pinMode(VOLUME_EN2, OUTPUT);
#else
    // Power State
    pinMode(POW_STA, INPUT);
#endif

    digitalWrite(VOLUME_EN1, LOW);
    pinMode(VOLUME_EN1, OUTPUT);

    // Battery check
    analogReference(AR_INTERNAL1V65);
    pinMode(BATT_AD, INPUT);

    // for LTE-M
    pinMode(RF_CTS_IN, INPUT);
    pinMode(RF_WAKE_INT_IN, INPUT);
#ifdef USE_LTE_M_CTS
    digitalWrite(RF_WAKEUP_OUT, HIGH);
    pinMode(RF_WAKEUP_OUT, OUTPUT);
    digitalWrite(RF_RESET_OUT, HIGH);
    pinMode(RF_RESET_OUT, OUTPUT);
#else
    pinMode(RF_WAKEUP_OUT, INPUT);
    pinMode(RF_RESET_OUT, INPUT);
#endif

    // Start internal I2C
    I2C_internal.begin(); // 100kHz frequency

    // BME280
    SensorDm.setI2CAddress(BME280_I2C_ADD);
    if (SensorDm.beginI2C(I2C_internal) == false)
    {
        SerialDebug.println("Sensor connect failed");
        return false;
    }

    // ADC
#ifdef MINI_V2
    adcDm.begin(MCP3221_I2C_ADD, I2C_internal);
#else
    adcDm.begin(MCP3221_I2C_ADD, I2C_internal,3.3F);
#endif

    // RTC
    bool status = rtcDm.begin(RX8130_I2C_ADD, I2C_internal);
    if (status == false)
    {
        SerialDebug.println("Could not find a valid rtcDm8130, check wiring!");
        return false;
    }
    else
    {
        rtcValidAtBoot = rtcDm._valid;
    }

    return true;
}

bool DOCODEMO::LedCtrl(uint8_t led, uint8_t onoff)
{
    bool ret = false;

    if ((led != RED_LED && led != GREEN_LED) || onoff > 1)
    {
        return ret;
    }

    // !!! ON/HIGH ->turn off, OFF/LOW-> turn on , so invert it.
    onoff = (~onoff) & 0x01;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        if (led == RED_LED)
        {
            digitalWrite(RED_LED, onoff);
        }
        else
        {
            digitalWrite(GREEN_LED, onoff);
        }
        xSemaphoreGive(xSemaphoreDocodemo);
        ret = true;
    }

    return ret;
}

bool DOCODEMO::ModemPowerCtrl(uint8_t onoff)
{
    bool ret = false;

    if (onoff != ON && onoff != OFF)
    {
        return ret;
    }

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        digitalWrite(MODEM_POWER_PORT, onoff);

        xSemaphoreGive(xSemaphoreDocodemo);
        ret = true;
    }
    return ret;
}

float DOCODEMO::readPressure()
{
    float val = 0;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        val = SensorDm.readFloatPressure();
        xSemaphoreGive(xSemaphoreDocodemo);
    }
    return val;
}

float DOCODEMO::readHumidity()
{
    float val = 0;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        val = SensorDm.readFloatHumidity();
        xSemaphoreGive(xSemaphoreDocodemo);
    }
    return val;
}

float DOCODEMO::readTemperature()
{
    float val = 0;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        val = SensorDm.readTempC();
        xSemaphoreGive(xSemaphoreDocodemo);
    }
    return val;
}

float DOCODEMO::readExADC()
{
    float val = 0;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        val = adcDm.readADC();
    }
    xSemaphoreGive(xSemaphoreDocodemo);
    return val;
}

int DOCODEMO::readExIN()
{
    int rv = -1;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        rv = digitalRead(EXGPIO_PORT_INPUT_0) ? 0 : 1;
        xSemaphoreGive(xSemaphoreDocodemo);
    }
    return rv;
}

bool DOCODEMO::exOutCtrl(uint8_t onoff)
{
    bool rv = false;

    if (onoff > 1)
    {
        return rv;
    }

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        digitalWrite(EXGPIO_PORT_OUTPUT_0, onoff);
        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
    }

    return rv;
}

bool DOCODEMO::setPwm(uint16_t helz)
{
    bool rv = false;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        tone(BEEP_PWM, helz);

        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
    }
    return rv;
}

bool DOCODEMO::BeepVolumeCtrl(uint8_t vol)
{
    bool rv = false;

    if (vol > 3)
    {
        return rv;
    }

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
#ifdef MINI_V2
        digitalWrite(VOLUME_EN1, vol & 0x1);
        digitalWrite(VOLUME_EN2, (vol >> 1));
#else
        if (vol)
        {
            digitalWrite(VOLUME_EN1, 1);
        }
        else
        {
            digitalWrite(VOLUME_EN1, 0);
        }
#endif
        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
    }

    return rv;
}

bool DOCODEMO::exPowerCtrl(uint8_t onoff)
{
    bool rv = false;

    if (onoff != ON && onoff != OFF)
    {
        return rv;
    }

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        digitalWrite(EX_POWER_PORT, onoff);
        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
    }

    return rv;
}

#ifdef MINI_V2
bool DOCODEMO::exUartPowerCtrl(uint8_t onoff)
{
    bool rv = false;

    if (onoff != ON && onoff != OFF)
    {
        return rv;
    }

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        digitalWrite(EX_UART_POWER_PORT, onoff);
        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
    }

    return rv;
}

bool DOCODEMO::exI2CPowerCtrl(uint8_t onoff)
{
    bool rv = false;

    if (onoff != ON && onoff != OFF)
    {
        return rv;
    }

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        digitalWrite(EX_I2C_POWER_PORT, onoff);
        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
    }

    return rv;
}

#else

bool DOCODEMO::exComPowerCtrl(uint8_t onoff)
{
    bool rv = false;

    if (onoff != ON && onoff != OFF)
    {
        return rv;
    }

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        digitalWrite(EXCOM_ONOFF, onoff);
        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
    }

    return rv;
}

#endif

DateTime DOCODEMO::rtcNow()
{
    DateTime now = DateTime(0, 0, 0);

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        now = rtcDm.now();
        xSemaphoreGive(xSemaphoreDocodemo);
    }
    return now;
}

bool DOCODEMO::rtcAdjust(const DateTime &dt)
{
    bool rv = false;
    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        rtcDm.adjust(dt);
        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
        rtcAdjusted = true;
    }
    return rv;
}

bool DOCODEMO::sleep(SleepMode mode)
{
    bool rv = false;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        //if (!_sleep_initialized)
        //    _initialize_sleep();

        switch (mode)
        {
        case SleepMode::IDLE0:
        case SleepMode::IDLE1:
        case SleepMode::IDLE2:
            SCB->SCR &= ~SCB_SCR_SLEEPDEEP_Msk;
            PM->SLEEP.reg = (uint8_t)mode;
            rv = true;
            break;
        case SleepMode::DEEPSLEEP:
            // for powersave
            SensorDm.setMode(MODE_SLEEP);

            SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;
            rv = true;
            break;
        default:
            break;
        }

        if (rv)
        {
            // Disable systick interrupt: errata 1.5.7 Potential Lockup on Standby Entry
            if (mode == SleepMode::DEEPSLEEP)
                SysTick->CTRL &= ~SysTick_CTRL_TICKINT_Msk;
            __DSB();
            __WFI();
            // Enable systick interrupt
            if (mode == SleepMode::DEEPSLEEP)
                SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;

            if (mode == SleepMode::DEEPSLEEP)
            {
                SensorDm.setMode(MODE_NORMAL);
            }
        }

        xSemaphoreGive(xSemaphoreDocodemo);
    }

    return rv;
}

void dumycall()
{

}

bool DOCODEMO::setRtcTimer(const RtcTimerPeriod period, const uint16_t count, voidFuncPtr callback)
{
    bool rv = false;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        rtcDm.setTimer(period, count);

#ifdef MINI_V2
        if (callback != nullptr)
        {
            attachInterrupt(RTC_IRQ, callback, FALLING);
        }
        else
        {
            attachInterrupt(RTC_IRQ, NULL, FALLING);
        }
#else
        if (callback != nullptr)
        {
            attachInterrupt(RTC_IRQ, callback, RISING);
        }
        else
        {
            attachInterrupt((RTC_IRQ), dumycall, RISING);
        }
#endif
    	_initialize_sleep();
        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
    }

    return rv;
}

bool DOCODEMO::chkRtcTimer(void)
{
    bool rv = false;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        rv = rtcDm.chkTimer();
        xSemaphoreGive(xSemaphoreDocodemo);
    }

    return rv;
}

bool DOCODEMO::stopRtcTimer(void)
{
    bool rv = false;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        detachInterrupt(digitalPinToInterrupt(RTC_IRQ));
        rtcDm.stopTimer();
        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
    }

    return rv;
}

bool DOCODEMO::chkRtcBattery(void)
{
    bool rv = false;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        rv = rtcDm.chkBattery();
        xSemaphoreGive(xSemaphoreDocodemo);
    }

    return rv;
}

float DOCODEMO::readBatteryVoltage(void)
{
    float rv = 0;

#ifdef MINI_V2
    int volt = analogRead(BATT_AD);
#else
    digitalWrite(BATMON_ONOFF, HIGH);
	delay(20);//wait charge
    int volt = analogRead(BATT_AD);
    digitalWrite(BATMON_ONOFF, LOW);
#endif
    rv = (float)volt / 1024.0 * 1.65 * 10.0;
	rv += 0.2; //add diode loss value

    return rv;
}

bool DOCODEMO::writeRtcRam(uint32_t val)
{
    bool rv = false;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        rtcDm.writeRAM(val);
        rv = true;
        xSemaphoreGive(xSemaphoreDocodemo);
    }

    return rv;
}

uint32_t DOCODEMO::readRtcRam(void)
{
    uint32_t rv = 0;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        rv = rtcDm.readRAM();

        xSemaphoreGive(xSemaphoreDocodemo);
    }

    return rv;
}

bool DOCODEMO::setAlarm(u_int8_t mode, uint8_t hour, uint8_t min)
{
    bool rv = false;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        rtcDm.setAlarm(mode, hour, min);
        rv = true;
        xSemaphoreGive(xSemaphoreDocodemo);
    }

    return rv;
}

bool DOCODEMO::stopAlarm(void)
{
    bool rv = false;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        rtcDm.stopAlarm();
        rv = true;
        xSemaphoreGive(xSemaphoreDocodemo);
    }

    return rv;
}

void DOCODEMO::reset(void)
{
    SCB->AIRCR = ((0x5FA << SCB_AIRCR_VECTKEY_Pos) | SCB_AIRCR_SYSRESETREQ_Msk);
}

int DOCODEMO::wdt_enable(int maxPeriodMS)
{
    int cycles;
    uint8_t bits;

    if (!_wdt_initialized)
        _initialize_wdt();

    WDT->CTRL.reg = 0; // Disable watchdog for config
    while (WDT->STATUS.bit.SYNCBUSY)
        ;

    if ((maxPeriodMS >= 16000) || !maxPeriodMS)
    {
        cycles = 16384;
        bits = 0xB;
    }
    else
    {
        cycles = (maxPeriodMS * 1024L + 500) / 1000; // ms -> WDT cycles
        if (cycles >= 8192)
        {
            cycles = 8192;
            bits = 0xA;
        }
        else if (cycles >= 4096)
        {
            cycles = 4096;
            bits = 0x9;
        }
        else if (cycles >= 2048)
        {
            cycles = 2048;
            bits = 0x8;
        }
        else if (cycles >= 1024)
        {
            cycles = 1024;
            bits = 0x7;
        }
        else if (cycles >= 512)
        {
            cycles = 512;
            bits = 0x6;
        }
        else if (cycles >= 256)
        {
            cycles = 256;
            bits = 0x5;
        }
        else if (cycles >= 128)
        {
            cycles = 128;
            bits = 0x4;
        }
        else if (cycles >= 64)
        {
            cycles = 64;
            bits = 0x3;
        }
        else if (cycles >= 32)
        {
            cycles = 32;
            bits = 0x2;
        }
        else if (cycles >= 16)
        {
            cycles = 16;
            bits = 0x1;
        }
        else
        {
            cycles = 8;
            bits = 0x0;
        }
    }

    WDT->INTENCLR.bit.EW = 1;   // Disable early warning interrupt
    WDT->CONFIG.bit.PER = bits; // Set period for chip reset
    WDT->CTRL.bit.WEN = 0;      // Disable window mode
    while (WDT->STATUS.bit.SYNCBUSY)
        ; // Sync CTRL write

    wdt_reset();              // Clear watchdog interval
    WDT->CTRL.bit.ENABLE = 1; // Start watchdog now!
    while (WDT->STATUS.bit.SYNCBUSY)
        ;

    return (cycles * 1000L + 512) / 1024; // WDT cycles -> ms
}

void DOCODEMO::wdt_reset()
{
    while (WDT->STATUS.bit.SYNCBUSY)
        ;
    WDT->CLEAR.reg = WDT_CLEAR_CLEAR_KEY;
}

void DOCODEMO::wdt_disable(void)
{
    WDT->CTRL.bit.ENABLE = 0;
    while (WDT->STATUS.bit.SYNCBUSY)
        ;
}
void WDT_Handler(void)
{
    WDT->CTRL.bit.ENABLE = 0; // Disable watchdog
    while (WDT->STATUS.bit.SYNCBUSY)
        ;                    // Sync CTRL write
    WDT->INTFLAG.bit.EW = 1; // Clear interrupt flag
}

void DOCODEMO::_initialize_wdt()
{
    // Generic clock generator 2, divisor = 32 (2^(DIV+1))
    GCLK->GENDIV.reg = GCLK_GENDIV_ID(2) | GCLK_GENDIV_DIV(4);
    // Enable clock generator 2 using low-power 32KHz oscillator.
    // With /32 divisor above, this yields 1024Hz(ish) clock.
    GCLK->GENCTRL.reg = GCLK_GENCTRL_ID(2) | GCLK_GENCTRL_GENEN |
                        GCLK_GENCTRL_SRC_OSCULP32K | GCLK_GENCTRL_DIVSEL;
    while (GCLK->STATUS.bit.SYNCBUSY)
        ;
    // WDT clock = clock gen 2
    GCLK->CLKCTRL.reg =
        GCLK_CLKCTRL_ID_WDT | GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK2;

    // Enable WDT early-warning interrupt
    NVIC_DisableIRQ(WDT_IRQn);
    NVIC_ClearPendingIRQ(WDT_IRQn);
    NVIC_SetPriority(WDT_IRQn, 0); // Top priority
    NVIC_EnableIRQ(WDT_IRQn);

    _wdt_initialized = true;
}

void DOCODEMO::_initialize_sleep()
{
    SYSCTRL->XOSC32K.reg |= (SYSCTRL_XOSC32K_RUNSTDBY | SYSCTRL_XOSC32K_ONDEMAND); // set external 32k oscillator to run when idle or sleep mode is chosen
    REG_GCLK_CLKCTRL |= GCLK_CLKCTRL_ID(GCM_EIC) |                                 // generic clock multiplexer id for the external interrupt controller
                        GCLK_CLKCTRL_GEN_GCLK1 |                                   // generic clock 1 which is xosc32k
                        GCLK_CLKCTRL_CLKEN;                                        // enable it
    while (GCLK->STATUS.bit.SYNCBUSY)
        ;
	
    // errata 1.14.2 Power-down Modes and Wake-up From Sleep
    NVMCTRL->CTRLB.bit.SLEEPPRM = NVMCTRL_CTRLB_SLEEPPRM_DISABLED_Val;
	
    _sleep_initialized = true;
}

#ifndef MINI_V2
bool DOCODEMO::e2prom_write(uint16_t add, uint8_t data)
{
    bool rv = false;

    if (add < 8192 && xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        digitalWrite(SD_PW, LOW); // Write Protect off for E2PROM
        //If set HIGH, SD card will be power on... please keep LOW for power consumption.
        //When use SD card ,E2PROM do not allow to write date because Write Protect is ON.

        I2C_internal.beginTransmission(BR24G64_I2C_ADD);
        I2C_internal.write((uint8_t)(add >> 8));   // MSB
        I2C_internal.write((uint8_t)(add & 0xFF)); // LSB
        I2C_internal.write(data);
        I2C_internal.endTransmission();

        delay(5);                 // for stable

        rv = true;
        xSemaphoreGive(xSemaphoreDocodemo);
    }

    return rv;
}

bool DOCODEMO::e2prom_read(uint16_t add, uint8_t *data)
{
    bool rv = false;

    if (add < 8192 && xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        byte rdata = 0xFF;
        
        I2C_internal.beginTransmission(BR24G64_I2C_ADD);
        I2C_internal.write((uint8_t)(add >> 8));   // MSB
        I2C_internal.write((uint8_t)(add & 0xFF)); // LSB
        I2C_internal.endTransmission();

        I2C_internal.requestFrom(BR24G64_I2C_ADD, 1);
        if (I2C_internal.available())
        {
            rdata = I2C_internal.read();
            rv = true;
        }

        *data = rdata;

        xSemaphoreGive(xSemaphoreDocodemo);
    }

    return rv;
}
#endif