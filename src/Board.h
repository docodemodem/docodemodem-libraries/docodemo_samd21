/*
 * Include header for DocodeModem Mini depend on hardware
 * Copyright (c) 2021 Circuit Desgin,Inc
 * Released under the MIT license
 */
#ifndef _BOARD_H_
#define _BOARD_H_

/*======================================================================
 *  Include File
 *====================================================================*/
#include <Arduino.h>
// to avoid error: macro “min” passed 3 arguments, but takes just 2
#undef max
#undef min
#include <Wire.h>
#include <FreeRTOS_SAMD21.h>
// #define delay(x) vTaskDelay(x)

/*======================================================================
 *	#define for Global
 *====================================================================*/
#define _DOCODE_MODEM_MINI 3

// GPIO Number
#ifndef FEATHER
#define EXTERNAL_SDA 2
#define MODEM_POWER_PORT 4
#define INTERNAL_SDA 16
#define INTERNAL_SCL 17
#define SPI_MISO 18 // SD DAT0(MISO)
#define SPI_SCK 20  // SD CLK(SCK)
#define SPI_MOSI 21 // SD CMD(MOSI)
#define SD_CS 24
#define RF_WAKE_INT_IN 26 // for LTE-M
#define EXGPIO_PORT_INPUT_0 27
#define EXGPIO_PORT_OUTPUT_0 28
#define GREEN_LED 30
#define MAIN_PWR_CNT_OUT 31
#define RTC_IRQ 43
#define EX_UART_TX_PORT 35
#define EX_UART_RX_PORT 36
#define RF_RESET_OUT 25 // for LTE-M
#else                   // FEATHER
#define EXTERNAL_SDA 4
#define MODEM_POWER_PORT 2
#define INTERNAL_SDA 20
#define INTERNAL_SCL 21
#define SPI_MISO 22 // SD DAT0(MISO)
#define SPI_SCK 24  // SD CLK(SCK)
#define SPI_MOSI 23 // SD CMD(MOSI)
#define SD_CS 14
#define RF_WAKE_INT_IN 16 // for LTE-M
#define EXGPIO_PORT_INPUT_0 17
#define EXGPIO_PORT_OUTPUT_0 18
#define GREEN_LED 25
#define MAIN_PWR_CNT_OUT 26
#define RTC_IRQ 38
#define EX_UART_TX_PORT 30
#define EX_UART_RX_PORT 31
#define RF_RESET_OUT 15 // for LTE-M
#endif                  // FEATHER

#ifdef MINI_V2
#define VOLUME_EN2 6
#define EX_UART_POWER_PORT 5
#define EX_I2C_POWER_PORT 11
#define SD_SW 32 // SD exist
#else            // MINI_V2
#define POW_STA 6
#define BATMON_ONOFF 5
#define EXCOM_ONOFF 11
#ifdef FEATHER
#define SD_PW 27 // SD Power On/Off
#else
#define SD_PW 32 // SD Power On/Off
#endif           // FEATER
#endif           // MINI_V2

#define RED_LED 13
#define VOLUME_EN1 12
#define BEEP_PWM 7
#define EX_POWER_PORT 10
#define MODEM_UART_TX 1 // To Rx of modeule
#define MODEM_UART_RX 0 // To TX of modeule
#define EXTERNAL_SCL 3
#define RF_CTS_IN 8     // for LTE-M
#define RF_WAKEUP_OUT 9 // for LTE-M
#define BATT_AD A5

// Other settings
#define MLR_BAUDRATE (19200)

#define BME280_I2C_ADD (0x76)
#define MCP3425_I2C_ADD (0x68) // AD
#define AD5693_I2C_ADD (0x4C)  // DA
#define ADP5589_I2C_ADD (0x34)
#define RX8130_I2C_ADD (0x32)  // RTC
#define MCP3221_I2C_ADD (0x4D) // AD
#define BR24G64_I2C_ADD (0x50) // E2PROM

#define TRUE (1)
#define FALSE (0)
#define ON (1)
#define OFF (0)

class DateTime
{
public:
    DateTime(uint16_t year, uint8_t month, uint8_t day,
             uint8_t hour = 0, uint8_t min = 0, uint8_t sec = 0);

    uint16_t year() const { return 2000 + y; }
    uint8_t month() const { return m; }
    uint8_t day() const { return d; }
    uint8_t hour() const { return hh; }
    uint8_t minute() const { return mm; }
    uint8_t second() const { return ss; }
    uint8_t dayOfWeek() const;

protected:
    uint8_t y, m, d, hh, mm, ss;
};

#endif // _BOARD_H_
