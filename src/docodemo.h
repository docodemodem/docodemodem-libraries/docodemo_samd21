/*
 * Include header for DocodeModem mini
 *
 * Copyright (c) 2021 Circuit Desgin,Inc
 * Released under the MIT license
 */

#pragma once

#include "Board.h"
// #include <Tone.h>
#include <SPI.h>
#include "mcp3221.h"
#include "rx8130.h"
#include "SparkFunBME280.h"

#ifndef FEATHER
#define UartExternal Serial
#if defined(USBCON)
#define SerialDebug SerialUSB
#endif
#else
#define UartExternal Serial5
#if defined(USBCON) || defined(USE_TINYUSB)
#define SerialDebug Serial
#define SerialUSB Serial
#endif
#endif

#define I2C_internal Wire
#define UartModem Serial1
extern TwoWire I2C_external;

class DOCODEMO
{
public:
    DOCODEMO(void);
    bool begin();

    bool LedCtrl(uint8_t led, uint8_t onoff);
    bool ModemPowerCtrl(uint8_t onoff);
    float readPressure();
    float readHumidity();
    float readTemperature();

    float readExADC();

    int readExIN();
    bool exOutCtrl(uint8_t onoff);

    bool setPwm(uint16_t val);
    bool BeepVolumeCtrl(uint8_t vol);

    bool exPowerCtrl(uint8_t onoff);
#ifdef MINI_V2
    bool exUartPowerCtrl(uint8_t onoff);
    bool exI2CPowerCtrl(uint8_t onoff);
#else
    bool exComPowerCtrl(uint8_t onoff);
    bool e2prom_write(uint16_t add, uint8_t data);
    bool e2prom_read(uint16_t add, uint8_t *data);
#endif
    DateTime rtcNow();
    bool rtcAdjust(const DateTime &dt);
    bool setRtcTimer(const RtcTimerPeriod period, uint16_t count, voidFuncPtr callback = nullptr);
    bool stopRtcTimer(void);
    bool rtcValidAtBoot = false;
    bool rtcAdjusted = false;
    bool sleep(SleepMode mode);
    bool chkRtcBattery(void);
    bool chkRtcTimer(void);
    bool writeRtcRam(uint32_t val);
    uint32_t readRtcRam(void);
    bool setAlarm(u_int8_t mode, uint8_t hour, uint8_t min);
    bool stopAlarm(void);

    float readBatteryVoltage(void);

    void reset(void);
    int wdt_enable(int maxPeriodMS);
    void wdt_reset();
    void wdt_disable(void);

private:
    void _initialize_wdt();
    bool _wdt_initialized;

    void _initialize_sleep();
    bool _sleep_initialized;
};

extern DOCODEMO Dm;
